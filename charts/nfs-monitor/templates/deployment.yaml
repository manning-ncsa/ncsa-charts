---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}
spec:
  ## Consider running as a DaemonSet or a Deployment with multiple replicas 
  ## (with anti-affinity to distribute among worker nodes) in order to alert
  ## for NFS mount issues that may affect only specific nodes
  replicas: 1
  selector:
    matchLabels:
      app: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}
    spec:
      volumes:
      - name: monitor-script
        configMap:
          name: monitor-script
      - name: condo-nfs-mount
        persistentVolumeClaim:
          {{- if .Values.persistence.existingClaim }}
          claimName: {{ .Values.persistence.existingClaim | quote }}
          {{- else }}
          claimName: nfs-monitor-pvc
          {{- end }}
      containers:
      - name: nfs-monitor
        image: registry.gitlab.com/gasc-ncsa/kubernetes/nfs-monitor:latest
        command: ['/bin/bash']
        args:
        - '/scripts/monitor.sh'
        env:
        - name: CLUSTER_NAME
          value: "{{ .Values.clusterName }}"
        - name: NFS_FOLDER_UUID
          value: "{{ .Values.nfsFolderUUID }}"
        - name: MATRIX_ROOM_ID
          value: "{{ .Values.matrix.room.roomId }}"
        - name: MATRIX_SERVER_URL
          value: "{{ .Values.matrix.server.baseUrl }}"
        - name: MATRIX_TOKEN
          {{- if and .Values.matrix.room.existingSecret .Values.matrix.room.secretKey }}
          valueFrom:
            secretKeyRef:
              name: "{{ .Values.matrix.room.existingSecret }}"
              key: "{{ .Values.matrix.room.secretKey }}"
          {{- else }}
          value: {{ .Values.matrix.room.accessToken | quote }}
          {{- end }}
        volumeMounts:
        - name: monitor-script
          subPath: monitor.sh
          mountPath: /scripts/monitor.sh
        - name: condo-nfs-mount
          mountPath: /mnt/nfs

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: monitor-script
data:
  monitor.sh: |
    {{- if .Values.monitorScript }}
    {{ .Values.monitorScript | nindent 4 }}
    {{- else }}
    #!/bin/bash
    #
    # Define Matrix room post API HTTP request function
    send_alert () {
      curl -s -S -k -XPOST \
        -d '{"msgtype":"m.text", "body":"'"$1"'"}' \
        "${MATRIX_SERVER_URL}/_matrix/client/r0/rooms/${MATRIX_ROOM_ID}/send/m.room.message?access_token=${MATRIX_TOKEN}"
    }
    #
    # Send alert that monitor is online
    #
    echo "NFS Monitor pod is online."
    send_alert "NFS Monitor Alert [${CLUSTER_NAME}]: NFS Monitor pod is online."
    #
    # Check that the expected folder is mounted
    #
    if [[ ! -e "/mnt/nfs/${NFS_FOLDER_UUID}" ]]; then
      echo "NFS volume not mounted!" 1>&2
      send_alert "NFS Monitor Alert [${CLUSTER_NAME}]: NFS volume not mounted!"
      exit 1
    fi
    #
    # Periodically write to an NFS-mounted file to test that the NFS system is still connected
    #
    while true
    do
      echo $(date) > /mnt/nfs/nfs-monitor.log || break
      sleep 60
    done
    #
    # If there is an error and the while loop breaks, send an alert
    #
    echo "Error writing to NFS mount!" 1>&2
    send_alert "NFS Monitor Alert [${CLUSTER_NAME}]: Unable to write to NFS-mounted volume!"
    #
    # To avoid repeated error alerts caused by the automatic pod restart, 
    # loop indefinitely while sending a daily reminder alert that the NFS system is down.
    #
    while true
    do
      echo "$(date): NFS Monitor is in an error state. Please resolve the problem and then restart the monitor."
      sleep 1d
    done
    {{- end }}
