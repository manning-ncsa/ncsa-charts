NCSA Radiant NFS storage system monitor
============================================

Persistent Volume
--------------------------------------------

You must manually create a directory in your Radiant project's NFS-mounted storage. Mount the drive:
```sh
[root@cluster-worker-0]# mkdir -p /mnt/condo
[root@cluster-worker-0]# mount radiant-nfs.ncsa.illinois.edu:/radiant/projects/project-code/cluster-name /mnt/condo
```

Then create the folder for the PV as well as the unique filename for identification.
```sh
[root@cluster-worker-0]# mkdir /mnt/condo/nfs-monitor
[root@cluster-worker-0]# touch /mnt/condo/nfs-monitor/f688218c664cc051dd0c1705e91c003e7b
```

Alerts
--------------------------------------------

The monitor will send alert messages to a [Matrix room](https://matrix.org/) using a supplied configuration that includes the Matrix server URL and the room ID. Additionally, you will need to create a Secret containing the access token for the Matrix account that has the relevant permissions to post messages to the specified room. Details about the Matrix API can be found in [the Matrix documentation](https://www.matrix.org/docs/guides/client-server-api).
